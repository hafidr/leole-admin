import { useState, useEffect, useCallback, useMemo, useRef } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import qs from "qs";
import withSession from "../lib/session";
import { toastAlert, MySwal } from "../lib/sweetalert";
import Layout from "../components/Layout";
import PageHeader from "../components/PageHeader";
import Modal from "../components/Modal";
import Datatable from "../components/Datatable";
import getCurrentPrivilege from "../lib/getCurrentPrivilege";

export const SellingPrice = ({ user, privilege, ACL }) => {

    axios.defaults.headers.common['authkey'] = user.authKey;
    const CancelToken   = axios.CancelToken;
    const fetchToken    = CancelToken.source();
    const router        = useRouter();

    const API_URL       = process.env.API_ENDPOINT + "api/selling-price";
    const columns   = useMemo(
        () => [
            {
                Header: '#',
                accessor: '_index_',
                disableFilters: true
            },
            {
                Header: 'Nominal 1',
                accessor: 'nominal_1',
            },
            {
                Header: 'Operation',
                accessor: 'operation',
            },
            {
                Header: 'Nominal 2',
                accessor: 'nominal_2',
            },
            {
                Header: 'Persen',
                accessor: 'persen',
            },
            {
                Header: 'Action',
                accessor: 'action',
                disableFilters: true,
                disableSortBy: true
            },
        ], []
    );
    const INITIAL_FORM  = {
        id: "",
        nominal_1: "",
        operation: "",
        nominal_2: "",
        persen: ""
    }

    const [modalShow, setModalShow]         = useState(false);
    const [form, setForm]                   = useState(INITIAL_FORM);
    const [formOption, setFormOption]       = useState({ title: "Tambah Data", method: "POST" });
    const [isFormSubmit, setIsFormSubmit]   = useState(false);

    const [data, setData]           = useState([]);
    const [loading, setLoading]     = useState(false);
    const [reload, setReload]       = useState(0);
    const [pageCount, setPageCount] = useState(0);
    
    const fetchIdRef                = useRef(0);
    
    const toggleModal = ({ resetForm = true } = {}) => {
        if (resetForm === true) setForm(INITIAL_FORM);
        setModalShow(state => !state);
    }
    
    const openCreateModal = () => {
        setFormOption({ title: "Tambah Data", method: "POST" });
        toggleModal();
    }
    
    const openUpdateModal = async (id) => {
        MySwal.fire({
            title: "Menunggu",
            text: "Mengambil Data...",
            didOpen: () => {
                MySwal.showLoading()
            }
        });
        
        try {
            const request = await axios({
                url: API_URL + "?id=" + id,
                method: "GET"
            });

            const response = await request.data;
            MySwal.close();
            
            if (response.status_code === 200) {
                setFormOption({ title: "Edit Data", method: "PUT" });
                setForm({
                    id: response.data.id,
                    nominal_1: response.data.nominal_1,
                    operation: response.data.operation,
                    nominal_2: response.data.nominal_2,
                    persen: response.data.persen
                })
                toggleModal({ resetForm: false });
            } else {
                const error         = new Error(response.status_code);
                error.status_code   = response.status_code;
                error.message       = response.message;

                throw error;
            }
        } catch (error) {
            toastAlert("error", error.message || "Internal Server Error!");
        }
    }

    const openDeleteModal = (id) => {
        MySwal.fire({
            title: 'Anda Yakin?',
            text: "Data Akan Dihapus Permanen!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
          }).then(async (result) => {
            if (result.isConfirmed) {
                MySwal.fire({
                    title: "Menunggu",
                    text: "Menghapus Data...",
                    didOpen: () => {
                        MySwal.showLoading()
                    }
                });
    
                try {
                    const request = await axios({
                        url: API_URL + "?id=" + id,
                        method: "DELETE"
                    });
        
                    const response = await request.data;
                    
                    if (response.status_code === 200) {
                        MySwal.close();
                        toastAlert("success", response.message, 10);
                        setTimeout(() => {
                            refreshTable();
                        }, 10);
                    } else {
                        const error         = new Error(response.status_code);
                        error.status_code   = response.status_code;
                        error.message       = response.message;
        
                        throw error;
                    }
                } catch (error) {
                    toastAlert("error", error.message || "Internal Server Error!");
                }
            }
        })
    }

    const formSubmitHandler = async (e) => {
        e.preventDefault();
        
        setIsFormSubmit(true);
        try {
            const url = (formOption.method === "PUT") 
                        ? API_URL + "?id=" + form.id
                        : API_URL;

            const request = await axios({
                url: url,
                method: formOption.method,
                data: qs.stringify({ ...form })
            });

            const response = await request.data;

            if (response.status_code === 200) {
                toggleModal();
                toastAlert("success", response.message, 10);
                setTimeout(() => {
                    refreshTable();
                }, 10);
            } else {
                const error         = new Error(response.status_code);
                error.status_code   = response.status_code;
                error.message       = response.message;

                throw error;
            }
        } catch (error) {
            toastAlert("error", error.message || "Internal Server Error!");
        }
        setIsFormSubmit(false);
    }

    const refreshTable = () => {
        toastAlert("info", "Mengambil Data!", 1000);
        setReload(Math.random());
    }

    const fetchData   = useCallback(async ({ pageIndex, pageSize, filters }) => {
        const fetchId = ++fetchIdRef.current;
        setLoading(true);

        try {
            const page_query    = "?page=" + (pageIndex + 1);
            const limit_query   = "&limit=" + pageSize;
            let criteria        = "&criteria=";
            let criteria_query  = "";
            let i               = 1;
            for (const filter of filters) {
                // id === key;
                criteria_query += `${filter.id}:${filter.value}`;
                if (i !== filters.length) {
                    criteria_query += ",";
                } else {
                    criteria_query = criteria + criteria_query;
                }

                i++;
            }

            const request = await axios({
                method: "GET",
                url: API_URL + page_query + limit_query + criteria_query,
                cancelToken: fetchToken.token
            });

            const response = await request.data;

            if (response.status_code === 401) {
                await axios({
                    method: "GET",
                    url: "/api/logout"
                });

                router.push("/login");
            }

            if (fetchId === fetchIdRef.current & response.status_code === 200) {
                const resData       =   response.data;
                const readyData     =   resData.map((value, index) => {
                    const update_button =   (ACL.can_update === true)
                                                ?   <button onClick={() => openUpdateModal(value.id)} className="px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none">
                                                        <i className="fas fa-pencil-alt" />
                                                    </button>  :   null;
                    const delete_button =   (ACL.can_delete === true)
                                                ?   <button onClick={() => openDeleteModal(value.id)} className="px-4 py-2 bg-red-700 dark:bg-red-800 hover:bg-red-800 dark:hover:bg-red-700 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none">
                                                        <i className="fas fa-trash-alt" />
                                                    </button>  :   null;
                    value._index_   =   (index + 1) + (pageIndex * pageSize);
                    value.action    =   <div className="inline-flex gap-2">
                                            { update_button }
                                            { delete_button }
                                        </div>
                    return value;
                });
                
                setData(readyData);
                setPageCount(Math.ceil(response.total_data / pageSize));
                setLoading(false);
            } else {
                if (response.status_code !== 200) toastAlert("error", response.message);
                setData([]);
                setPageCount(0);
                setLoading(false);
            }
        } catch (error) {
            toastAlert("error", error.message);
        }
    }, []);

    useEffect(() => {
        return () => {
            fetchToken.cancel();
        }
    }, []);

    return (
        <Layout privilege={privilege}>
            <PageHeader title="Harga Jual" />
            <section className="p-3 md:p-5">
                <div className="w-full rounded-lg bg-white dark:bg-gray-800 shadow p-5 md:p-8">
                    <div className="flex items-center justify-between mb-4">
                        <div className="flex-grow">
                            { (ACL.can_create === true) ? (
                                <button
                                    type="button"
                                    className="inline-flex items-center px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none"
                                    onClick={openCreateModal}
                                >
                                    <svg xmlns="http://www.w3.org/10/svg" className="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
                                    </svg>
                                    <span className="mx-px">Tambah Data</span>
                                </button>
                            ) : null }
                        </div>
                        <button onClick={refreshTable} className="inline-flex items-center px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none">
                            <svg xmlns="http://www.w3.org/10/svg" className="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z" clipRule="evenodd" />
                            </svg>
                        </button>
                    </div>
                    <div className="max-w-full">
                        <Datatable
                            columns={columns}
                            data={data}
                            fetchData={fetchData}
                            loading={loading}
                            reload={reload}
                            pageCount={pageCount}
                        />
                    </div>
                </div>
                <Modal title={formOption.title} isModalShow={modalShow} modalHandler={setModalShow}>
                    <form onSubmit={formSubmitHandler}>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Nominal 1</label>
                            <div className="col-span-4">
                                <input
                                    type="number"
                                    name="nominal_1"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, nominal_1: e.target.value })}
                                    value={form.nominal_1 || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Operation</label>
                            <div className="col-span-4">
                                <input
                                    type="text"
                                    name="operation"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, operation: e.target.value })}
                                    value={form.operation || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Nominal 2</label>
                            <div className="col-span-4">
                                <input
                                    type="text"
                                    name="nominal_2"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, nominal_2: e.target.value })}
                                    value={form.nominal_2 || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Persen</label>
                            <div className="col-span-4">
                                <input
                                    type="text"
                                    name="persen"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, persen: e.target.value })}
                                    value={form.persen || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        
                        <div className="inline-flex items-center space-x-2">
                            <button
                                type="submit"
                                className="px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none"
                                disabled={isFormSubmit}
                            >
                                { isFormSubmit 
                                    ? <i className="fas fa-sync fa-spin" />
                                    : "Kirim" }
                            </button>
                            <button
                                type="button"
                                className="px-4 py-2 bg-gray-100 dark:bg-gray-700 hover:bg-gray-200 dark:hover:bg-gray-600 text-sm md:text-base text-gray-500 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none"
                                onClick={toggleModal}
                                disabled={isFormSubmit}
                            >
                                Batal
                            </button>
                        </div>
                    </form>
                </Modal>
            </section>
        </Layout>
    );
}

// Ambil data session sebelum page di render terus pass ke props
export const getServerSideProps = withSession(async ({ res, req, resolvedUrl }) => {
    const session_data = req.session.get("session");
    
    if ( ! session_data) {
        return {
            redirect : {
                destination: "/login",
                statusCode: 302
            } 
        };
    }

    const { user, privilege }   = session_data;
    const current_privilege     = getCurrentPrivilege(resolvedUrl.substr(1), privilege);
    
    return {
        props: {
            user,
            privilege,
            ACL: current_privilege
        }
    };
});

export default SellingPrice;