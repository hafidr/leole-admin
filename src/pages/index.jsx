import { useEffect } from "react";
import withSession from "../lib/session";
import Layout from '../components/Layout';
import PageHeader from "../components/PageHeader";
import getCurrentPrivilege from "../lib/getCurrentPrivilege";

export default function Home({ user, privilege }) {
	const hideHeroBanner = e => {
		const parentEl = e.target.parentElement;
		parentEl.classList.remove("opacity-100");
		parentEl.classList.add("duration-300", "opacity-0");

		setTimeout(() => {
		parentEl.remove();
		}, 350);
	}

	useEffect(() => {
		
	}, [])

	return (
		<Layout privilege={privilege}>
			<PageHeader title="Dashboard" />
			<section className="p-5">
				<div className="flex relative max-w-full h-auto bg-indigo-800 dark:bg-indigo-900 rounded-md shadow dark:shadow-lg p-6 md:p-8 space-x-5 mb-5 transition-opacity opacity-100">
					<button className="block text-2xl text-gray-300 font-extrabold absolute top-0 right-0 py-4 px-6 focus:outline-none" onClick={hideHeroBanner}>
						&times;
					</button>
				<div className="flex justify-items-center items-center">
					<i className="fas fa-rocket text-4xl md:text-6xl text-gray-200 dark:text-gray-300 text-center" />
				</div>
				<div className="flex-grow">
					<h1 className="block antialiased sm:subpixel-antialiased md:antialiased text-lg md:text-3xl text-gray-200 font-bold tracking-thight">Selamat Datang, { user.name }!</h1>
					<p className="block text-sm md:text-lg text-indigo-200 w-full md:w-1/2">
						Lorem Ipsum Sit Dolor Amet
					</p>
				</div>
				</div>
				<div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4 mb-5">
					<div className='flex-1 h-32 bg-white dark:bg-gray-800 rounded-md shadow-md'></div>
					<div className='flex-1 h-32 bg-white dark:bg-gray-800 rounded-md shadow-md'></div>
					<div className='flex-1 h-32 bg-white dark:bg-gray-800 rounded-md shadow-md'></div>
					<div className='flex-1 h-32 bg-white dark:bg-gray-800 rounded-md shadow-md'></div>
				</div>
				<div className="grid grid-cols-1 md:grid-cols-2 space-x-0 space-y-4 md:space-x-4 md:space-y-0 mb-5">
					<div className="flex flex-grow flex-col flex-nowrap space-y-5">
						
					</div>
					<div className="flex flex-grow flex-col flex-nowrap space-y-5">
						
					</div>
				</div>
			</section>
		</Layout>
	)
}

export const getServerSideProps = withSession(async ({ res, req }) => {
  const session_data = req.session.get("session");
  
	if ( ! session_data) {
		return {
			redirect : {
				destination: "/login",
				statusCode: 302
			} 
		};
	}

	const currentPrivilege = getCurrentPrivilege("Role", session_data.privilege);

	return {
		props: {
			user: session_data.user,
			privilege: session_data.privilege
		}
	};
});