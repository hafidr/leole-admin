import { useState, useEffect, useCallback, useMemo, useRef } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import classnames from "classnames";
import qs from "qs";
import withSession from "../lib/session";
import { toastAlert, MySwal } from "../lib/sweetalert";
import Layout from "../components/Layout";
import PageHeader from "../components/PageHeader";
import Modal from "../components/Modal";
import Datatable from "../components/Datatable";
import getCurrentPrivilege from "../lib/getCurrentPrivilege";
import serializedPriviageForm from "../lib/serializedPrivilageForm";

export const Privilege = ({ user, privilege, ACL, formData }) => {

    axios.defaults.headers.common['authkey'] = user.authKey;
    const CancelToken   = axios.CancelToken;
    const fetchToken    = CancelToken.source();
    const router        = useRouter();

    const API_URL       = process.env.API_ENDPOINT + "api/privilege";
    const columns   = useMemo(
        () => [
            {
                Header: '#',
                accessor: '_index_',
                disableFilters: true
            },
            {
                Header: 'Nama',
                accessor: 'name',
            },
            {
                Header: 'Deskripsi',
                accessor: 'description',
            },
            {
                Header: 'Action',
                accessor: 'action',
                disableFilters: true,
                disableSortBy: true
            },
        ], []
    );
    const INITIAL_FORM  = {
        id: "",
        role_id: "",
        privilege: []
    }

    const [modalShow, setModalShow]         = useState(false);
    const [form, setForm]                   = useState(INITIAL_FORM);
    const [formOption, setFormOption]       = useState({ title: "Tambah Data", method: "POST" });
    const [isFormSubmit, setIsFormSubmit]   = useState(false);

    const [data, setData]           = useState([]);
    const [loading, setLoading]     = useState(false);
    const [reload, setReload]       = useState(0);
    const [pageCount, setPageCount] = useState(0);
    
    const fetchIdRef                = useRef(0);
    
    const toggleModal = ({ resetForm = true } = {}) => {
        if (resetForm === true) setForm(INITIAL_FORM);
        setModalShow(state => !state);
    }
    
    const openCreateModal = () => {
        setFormOption({ title: "Tambah Data", method: "POST" });
        toggleModal();
    }
    
    const openUpdateModal = async (id) => {
        MySwal.fire({
            title: "Menunggu",
            text: "Mengambil Data...",
            didOpen: () => {
                MySwal.showLoading()
            }
        });
        
        try {
            const request = await axios({
                url: API_URL + "?id=" + id,
                method: "GET"
            });

            const response = await request.data;
            MySwal.close();
            
            if (response.status_code === 200) {
                setFormOption({ title: "Edit Data", method: "PUT" });
                setForm({
                    id: response.data.id,
                    role_id: id,
                    privilege: serializedPriviageForm(response.data.privilege)
                })
                toggleModal({ resetForm: false });
            } else {
                const error         = new Error(response.status_code);
                error.status_code   = response.status_code;
                error.message       = response.message;

                throw error;
            }
        } catch (error) {
            toastAlert("error", error.message || "Internal Server Error!");
        }
    }

    const formSubmitHandler = async (e) => {
        e.preventDefault();
        
        setIsFormSubmit(true);
        
        try {
            const url = API_URL;

            const request = await axios({
                url: url,
                method: "POST",
                data: qs.stringify({ ...form })
            });

            const response = await request.data;

            if (response.status_code === 200) {
                toggleModal();
                toastAlert("success", response.message, 2000);
                setTimeout(() => {
                    refreshTable();
                }, 2000);
            } else {
                const error         = new Error(response.status_code);
                error.status_code   = response.status_code;
                error.message       = response.message;

                throw error;
            }
        } catch (error) {
            toastAlert("error", error.message || "Internal Server Error!");
        }
        setIsFormSubmit(false);
    }

    const refreshTable = () => {
        toastAlert("info", "Mengambil Data!", 1000);
        setReload(Math.random());
    }

    const fetchData   = useCallback(async ({ pageIndex, pageSize, filters }) => {
        const fetchId = ++fetchIdRef.current;
        setLoading(true);

        try {
            const page_query    = "?page=" + (pageIndex + 1);
            const limit_query   = "&limit=" + pageSize;
            let criteria        = "&criteria=";
            let criteria_query  = "";
            let i               = 1;
            for (const filter of filters) {
                // id === key;
                criteria_query += `${filter.id}:${filter.value}`;
                if (i !== filters.length) {
                    criteria_query += ",";
                } else {
                    criteria_query = criteria + criteria_query;
                }

                i++;
            }

            const request = await axios({
                method: "GET",
                url: API_URL + page_query + limit_query + criteria_query,
                cancelToken: fetchToken.token
            });

            const response = await request.data;

            if (response.status_code === 401) {
                await axios({
                    method: "GET",
                    url: "/api/logout"
                });

                router.push("/login");
            }

            if (fetchId === fetchIdRef.current & response.status_code === 200) {
                const resData       =   response.data;
                const readyData     =   resData.map((value, index) => {
                    const update_button =   (ACL.can_update === true)
                                                ?   <button onClick={() => openUpdateModal(value.id)} className="px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none">
                                                        <i className="fas fa-pencil-alt" />
                                                    </button>  :   null;
                    value._index_   =   (index + 1) + (pageIndex * pageSize);
                    value.action    =   <div className="inline-flex gap-2">
                                            { update_button }
                                        </div>
                    return value;
                });
                
                setData(readyData);
                setPageCount(Math.ceil(response.total_data / pageSize));
                setLoading(false);
            } else {
                if (response.status_code !== 200) toastAlert("error", response.message);
                setData([]);
                setPageCount(0);
                setLoading(false);
            }
        } catch (error) {
            toastAlert("error", error.message);
        }
    }, []);

    const CreatePrivilegeForm = () => {
        return (
            <>
                <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                    <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Role</label>
                    <div className="col-span-4">
                        <select
                            name="role_id"
                            className="form-control"
                            onChange={(e) => setForm({ ...form, role_id: e.target.value })}
                            value={form.role_id || ""}
                        >
                            <option>-- Pilih --</option>
                            { formData?.roles?.map(role => <option key={role.id} value={role.id}>{ role.name }</option>) }
                        </select>
                    </div>
                </div>
                <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                    <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Menu</label>
                    <div className="col-span-4">
                        <select
                            name="menu_id"
                            className="form-control"
                            onChange={(e) => {
                                let state_copy = form.privilege;
                                state_copy[0] = {
                                    ...form.privilege[0],
                                    menu_id: e.target.value
                                }
                                setForm({ ...form, privilege: state_copy });
                            }}
                            value={form.privilege[0]?.menu_id || ""}
                        >
                            <option>-- Pilih --</option>
                            { formData?.menus?.map(menu => <option key={menu.id} value={menu.id}>{ menu.name }</option>) }
                        </select>
                    </div>
                </div>
                <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                    <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Izin Akses</label>
                    <div className="flex items-center space-x-5 text-sm py-2 col-span-4">
                        <label className="inline-flex items-center text-gray-700 dark:text-gray-300">
                            <input
                                type="radio"
                                name="can_read"
                                className="form-radio mr-1"
                                onChange={(e) => {
                                    let state_copy = form.privilege;
                                    state_copy[0] = {
                                        ...form.privilege[0],
                                        can_read: true
                                    }
                                    setForm({ ...form, privilege: state_copy });
                                }}
                                checked={form.privilege[0]?.can_read === true}
                            />
                            Ya
                        </label>
                        <label className="inline-flex items-center text-gray-700 dark:text-gray-300">
                            <input
                                type="radio"
                                name="can_read"
                                className="form-radio mr-1"
                                onChange={(e) => {
                                    let state_copy = form.privilege;
                                    state_copy[0] = {
                                        ...form.privilege[0],
                                        can_read: false
                                    }
                                    setForm({ ...form, privilege: state_copy });
                                }}
                                checked={form.privilege[0]?.can_read === false}
                            />
                            Tidak
                        </label>
                    </div>
                </div>
                <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                    <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Izin Menambah</label>
                    <div className="flex items-center space-x-5 text-sm py-2 col-span-4">
                        <label className="inline-flex items-center text-gray-700 dark:text-gray-300">
                            <input
                                type="radio"
                                name="can_create"
                                className="form-radio mr-1"
                                onChange={(e) => {
                                    let state_copy = form.privilege;
                                    state_copy[0] = {
                                        ...form.privilege[0],
                                        can_create: true
                                    }
                                    setForm({ ...form, privilege: state_copy });
                                }}
                                checked={form.privilege[0]?.can_create === true}
                            />
                            Ya
                        </label>
                        <label className="inline-flex items-center text-gray-700 dark:text-gray-300">
                            <input
                                type="radio"
                                name="can_create"
                                className="form-radio mr-1"
                                onChange={(e) => {
                                    let state_copy = form.privilege;
                                    state_copy[0] = {
                                        ...form.privilege[0],
                                        can_create: false
                                    }
                                    setForm({ ...form, privilege: state_copy });
                                }}
                                checked={form.privilege[0]?.can_create === false}
                            />
                            Tidak
                        </label>
                    </div>
                </div>
                <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                    <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Izin Mengubah</label>
                    <div className="flex items-center space-x-5 text-sm py-2 col-span-4">
                        <label className="inline-flex items-center text-gray-700 dark:text-gray-300">
                            <input
                                type="radio"
                                name="can_update"
                                className="form-radio mr-1"
                                onChange={(e) => {
                                    let state_copy = form.privilege;
                                    state_copy[0] = {
                                        ...form.privilege[0],
                                        can_update: true
                                    }
                                    setForm({ ...form, privilege: state_copy });
                                }}
                                checked={form.privilege[0]?.can_update === true}
                            />
                            Ya
                        </label>
                        <label className="inline-flex items-center text-gray-700 dark:text-gray-300">
                            <input
                                type="radio"
                                name="can_update"
                                className="form-radio mr-1"
                                onChange={(e) => {
                                    let state_copy = form.privilege;
                                    state_copy[0] = {
                                        ...form.privilege[0],
                                        can_update: false
                                    }
                                    setForm({ ...form, privilege: state_copy });
                                }}
                                checked={form.privilege[0]?.can_update === false}
                            />
                            Tidak
                        </label>
                    </div>
                </div>
                <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                    <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Izin Menghapus</label>
                    <div className="flex items-center space-x-5 text-sm py-2 col-span-4">
                        <label className="inline-flex items-center text-gray-700 dark:text-gray-300">
                            <input
                                type="radio"
                                name="can_delete"
                                className="form-radio mr-1"
                                onChange={(e) => {
                                    let state_copy = form.privilege;
                                    state_copy[0] = {
                                        ...form.privilege[0],
                                        can_delete: true
                                    }
                                    setForm({ ...form, privilege: state_copy });
                                }}
                                checked={form.privilege[0]?.can_delete === true}
                            />
                            Ya
                        </label>
                        <label className="inline-flex items-center text-gray-700 dark:text-gray-300">
                            <input
                                type="radio"
                                name="can_delete"
                                className="form-radio mr-1"
                                onChange={(e) => {
                                    let state_copy = form.privilege;
                                    state_copy[0] = {
                                        ...form.privilege[0],
                                        can_delete: false
                                    }
                                    setForm({ ...form, privilege: state_copy });
                                }}
                                checked={form.privilege[0]?.can_delete === false}
                            />
                            Tidak
                        </label>
                    </div>
                </div>
            </>
        );
    }

    const EditPrivilegeForm = () => {
        return (
            <div className="overflow-x-auto">  
                <table className="min-w-full divide-y divide-gray-200">
                    <thead>
                        <tr className="bg-indigo-800 dark:bg-indigo-900 text-gray-200 dark:text-gray-300">
                            <th className="px-6 py-3 text-left text-xs leading-4 font-medium uppercase tracking-wider">Menu</th>
                            <th className="px-6 py-3 text-left text-xs leading-4 font-medium uppercase tracking-wider">Izin Mengakses</th>
                            <th className="px-6 py-3 text-left text-xs leading-4 font-medium uppercase tracking-wider">Izin Menambah</th>
                            <th className="px-6 py-3 text-left text-xs leading-4 font-medium uppercase tracking-wider">Izin Mengupdate</th>
                            <th className="px-6 py-3 text-left text-xs leading-4 font-medium uppercase tracking-wider">Izin Menghapus</th>
                        </tr>
                    </thead>
                    <tbody className="bg-white dark:bg-gray-700 divide-y divide-gray-200">
                        { form?.privilege?.map((value, index) => (
                            <tr key={index} className={
                                classnames({ "bg-blue-800 dark:bg-blue-900": value.is_parent })
                            }>
                                <td className="px-6 py-3 text-gray-700 dark:text-gray-300 whitespace-nowrap">{value.label}</td>
                                <td className="px-6 py-3 text-gray-700 dark:text-gray-300 whitespace-nowrap">
                                    <input
                                        type="checkbox"
                                        className="form-checkbox border-2 rounded-sm"
                                        onChange={() => {
                                            let state_copy = form.privilege;
                                            state_copy[index].can_read = ! state_copy[index].can_read;
                                            setForm({ ...form, privilege: state_copy });
                                        }}
                                        checked={form.privilege[index].can_read === true}
                                        readOnly={(ACL?.can_create === false || ACL?.can_update === false)}
                                    />
                                </td>
                                <td className="px-6 py-3 text-gray-700 dark:text-gray-300 whitespace-nowrap">
                                    <input
                                        type="checkbox"
                                        className="form-checkbox border-2 rounded-sm"
                                        onChange={() => {
                                            let state_copy = form.privilege;
                                            state_copy[index].can_create = ! state_copy[index].can_create;
                                            setForm({ ...form, privilege: state_copy });
                                        }}
                                        checked={form.privilege[index].can_create === true}
                                        readOnly={(ACL?.can_create === false || ACL?.can_update === false)}
                                    />
                                </td>
                                <td className="px-6 py-3 text-gray-700 dark:text-gray-300 whitespace-nowrap">
                                    <input
                                        type="checkbox"
                                        className="form-checkbox border-2 rounded-sm"
                                        onChange={() => {
                                            let state_copy = form.privilege;
                                            state_copy[index].can_update = ! state_copy[index].can_update;
                                            setForm({ ...form, privilege: state_copy });
                                        }}
                                        checked={form.privilege[index].can_update === true}
                                        readOnly={(ACL?.can_create === false || ACL?.can_update === false)}
                                    />
                                </td>
                                <td className="px-6 py-3 text-gray-700 dark:text-gray-300 whitespace-nowrap">
                                    <input
                                        type="checkbox"
                                        className="form-checkbox border-2 rounded-sm"
                                        onChange={() => {
                                            let state_copy = form.privilege;
                                            state_copy[index].can_delete = ! state_copy[index].can_delete;
                                            setForm({ ...form, privilege: state_copy });
                                        }}
                                        checked={form.privilege[index].can_delete === true}
                                        readOnly={(ACL?.can_create === false || ACL?.can_update === false)}
                                    />
                                </td>
                            </tr>
                        )) }
                    </tbody>
                </table>
            </div>
        );
    }

    useEffect(() => {
        return () => {
            fetchToken.cancel();
        }
    }, []);

    return (
        <Layout privilege={privilege}>
            <PageHeader title="Privilege" />
            <section className="p-3 md:p-5">
                <div className="w-full rounded-lg bg-white dark:bg-gray-800 shadow p-5 md:p-8">
                    <div className="flex items-center justify-between mb-4">
                        <div className="flex-grow">
                            { (ACL.can_create === true) ? (
                                <button
                                    type="button"
                                    className="inline-flex items-center px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none"
                                    onClick={openCreateModal}
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" className="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
                                    </svg>
                                    <span className="mx-px">Tambah Data</span>
                                </button>
                            ) : null }
                        </div>
                        <button onClick={refreshTable} className="inline-flex items-center px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" className="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z" clipRule="evenodd" />
                            </svg>
                        </button>
                    </div>
                    <div className="max-w-full">
                        <Datatable
                            columns={columns}
                            data={data}
                            fetchData={fetchData}
                            loading={loading}
                            reload={reload}
                            pageCount={pageCount}
                        />
                    </div>
                </div>
                <Modal title={formOption.title} isModalShow={modalShow} modalHandler={setModalShow} xl>
                    <form onSubmit={formSubmitHandler}>
                        { formOption.method === "POST" ? <CreatePrivilegeForm /> : <EditPrivilegeForm /> }
                        <div className="inline-flex items-center space-x-2 mt-4">
                            <button
                                type="submit"
                                className="px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none"
                                disabled={isFormSubmit}
                            >
                                { isFormSubmit 
                                    ? <i className="fas fa-sync fa-spin" />
                                    : "Kirim" }
                            </button>
                            <button
                                type="button"
                                className="px-4 py-2 bg-gray-100 dark:bg-gray-700 hover:bg-gray-200 dark:hover:bg-gray-600 text-sm md:text-base text-gray-500 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none"
                                onClick={toggleModal}
                                disabled={isFormSubmit}
                            >
                                Batal
                            </button>
                        </div>
                    </form>
                </Modal>
            </section>
        </Layout>
    );
}

// Ambil data session sebelum page di render terus pass ke props
export const getServerSideProps = withSession(async ({ res, req, resolvedUrl }) => {
    const session_data = req.session.get("session");
    
    if ( ! session_data) {
        return {
            redirect : {
                destination: "/login",
                statusCode: 302
            } 
        };
    }

    const { user, privilege }   = session_data;
    const current_privilege     = getCurrentPrivilege(resolvedUrl.substr(1), privilege);
    let formData                = {};

    try {
        const requestRole = await axios({
            method: "GET",
            url: process.env.API_ENDPOINT + "api/role",
            headers: {
                "authKey": user.authKey
            }
        });
        const requestMenu = await axios({
            method: "GET",
            url: process.env.API_ENDPOINT + "api/menu",
            headers: {
                "authKey": user.authKey
            }
        });

        const responseRole = await requestRole.data;
        const responseMenu = await requestMenu.data;

        if (responseRole.status_code === 200 && responseMenu.status_code === 200) {
            formData.roles = responseRole.data;
            formData.menus = responseMenu.data;
        }
    } catch (error) {
        
    }
    
    return {
        props: {
            user,
            privilege,
            formData,
            ACL: current_privilege
        }
    };
});

export default Privilege;