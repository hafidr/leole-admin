import { useState, useEffect, useCallback, useMemo, useRef } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import withSession from "../lib/session";
import { toastAlert } from "../lib/sweetalert";
import Layout from "../components/Layout";
import PageHeader from "../components/PageHeader";
import Datatable from "../components/Datatable";
import getCurrentPrivilege from "../lib/getCurrentPrivilege";

export const Log = ({ user, privilege, ACL }) => {

    axios.defaults.headers.common['authkey'] = user.authKey;
    const CancelToken   = axios.CancelToken;
    const fetchToken    = CancelToken.source();
    const router        = useRouter();

    const API_URL       = process.env.API_ENDPOINT + "api/log";
    const columns   = useMemo(
        () => [
            {
                Header: '#',
                accessor: '_index_',
                disableFilters: true
            },
            {
                Header: 'Nama',
                accessor: 'user_name',
            },
            {
                Header: 'Method',
                accessor: 'method',
            },
            {
                Header: 'Subjek',
                accessor: 'subject',
            },
            {
                Header: 'Table',
                accessor: 'model',
            },
            {
                Header: 'Tanggal',
                accessor: 'created_at',
            }
        ], []
    );

    const [data, setData]           = useState([]);
    const [loading, setLoading]     = useState(false);
    const [reload, setReload]       = useState(0);
    const [pageCount, setPageCount] = useState(0);
    
    const fetchIdRef                = useRef(0);
    
    const refreshTable = () => {
        toastAlert("info", "Mengambil Data!", 1000);
        setReload(Math.random());
    }

    const fetchData   = useCallback(async ({ pageIndex, pageSize, filters }) => {
        const fetchId = ++fetchIdRef.current;
        setLoading(true);
        
        try {
            const page_query    = "?page=" + (pageIndex + 1);
            const limit_query   = "&limit=" + pageSize;
            let criteria        = "&criteria=";
            let criteria_query  = "";
            let i               = 1;
            for (const filter of filters) {
                // id === key;
                criteria_query += `${filter.id}:${filter.value}`;
                if (i !== filters.length) {
                    criteria_query += ",";
                } else {
                    criteria_query = criteria + criteria_query;
                }

                i++;
            }

            const request = await axios({
                method: "GET",
                url: API_URL + page_query + limit_query + criteria_query,
                cancelToken: fetchToken.token
            });

            const response = await request.data;

            if (response.status_code === 401) {
                await axios({
                    method: "GET",
                    url: "/api/logout"
                });

                router.push("/login");
            }

            if (fetchId === fetchIdRef.current & response.status_code === 200) {
                const resData       =   response.data;
                const readyData     =   resData.map((value, index) => {
                    value._index_       =   (index + 1) + (pageIndex * pageSize);
                    value.created_at    =   new Date(value.created_at).toLocaleDateString();
                    return value;
                });
                
                setData(readyData);
                setPageCount(Math.ceil(response.total_data / pageSize));
                setLoading(false);
            } else {
                if (response.status_code !== 200) toastAlert("error", response.message);
                setData([]);
                setPageCount(0);
                setLoading(false);
            }
        } catch (error) {
            toastAlert("error", error.message);
        }
    }, []);

    useEffect(() => {
        return () => {
            fetchToken.cancel();
        }
    }, []);

    return (
        <Layout privilege={privilege}>
            <PageHeader title="Log" />
            <section className="p-3 md:p-5">
                <div className="w-full rounded-lg bg-white dark:bg-gray-800 shadow p-5 md:p-8">
                    <div className="flex items-center justify-between mb-4">
                        <button onClick={refreshTable} className="inline-flex items-center px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" className="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z" clipRule="evenodd" />
                            </svg>
                        </button>
                    </div>
                    <div className="max-w-full">
                        <Datatable
                            columns={columns}
                            data={data}
                            fetchData={fetchData}
                            loading={loading}
                            reload={reload}
                            pageCount={pageCount}
                        />
                    </div>
                </div>
            </section>
        </Layout>
    );
}

// Ambil data session sebelum page di render terus pass ke props
export const getServerSideProps = withSession(async ({ res, req, resolvedUrl }) => {
    const session_data = req.session.get("session");
    
    if ( ! session_data) {
        return {
            redirect : {
                destination: "/login",
                statusCode: 302
            } 
        };
    }

    const { user, privilege }   = session_data;
    const current_privilege     = getCurrentPrivilege(resolvedUrl.substr(1), privilege);
    
    return {
        props: {
            user,
            privilege,
            ACL: current_privilege
        }
    };
});

export default Log;