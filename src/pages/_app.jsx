import 'regenerator-runtime/runtime';
import "../../styles/tailwind.global.css";
import "@fortawesome/fontawesome-free/css/all.min.css";

import Router from "next/router";
import NProgress from "nprogress";
import "nprogress/nprogress.css";

NProgress.configure({ 
    showSpinner: false
})

Router.events.on("routeChangeStart", () => NProgress.start())
Router.events.on("routeChangeComplete", () => NProgress.done())
Router.events.on("routeChangeError", () => NProgress.done())


export const App = ({ Component, pageProps })  =>  {
    return  (
        <Component {...pageProps} />
    )
}

export default App;