import { useState, useEffect, useCallback, useMemo, useRef } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import classnames from "classnames";
import withSession from "../lib/session";
import { toastAlert, MySwal } from "../lib/sweetalert";
import Layout from "../components/Layout";
import PageHeader from "../components/PageHeader";
import Modal from "../components/Modal";
import Datatable from "../components/Datatable";
import getCurrentPrivilege from "../lib/getCurrentPrivilege";

export const Product = ({ user, privilege, ACL, formData }) => {

    axios.defaults.headers.common['authkey'] = user.authKey;
    const CancelToken   = axios.CancelToken;
    const fetchToken    = CancelToken.source();
    const router        = useRouter();

    const API_URL       = process.env.API_ENDPOINT + "api/product";
    const columns   = useMemo(
        () => [
            {
                Header: '#',
                accessor: '_index_',
                disableFilters: true
            },
            {
                Header: 'Photo',
                accessor: 'image',
                disableFilters: true,
                disableSortBy: true
            },
            {
                Header: 'Nama',
                accessor: 'name',
            },
            {
                Header: 'Kategori',
                accessor: 'category.name',
            },
            {
                Header: 'Harga',
                accessor: 'price',
            },
            {
                Header: 'Stok',
                accessor: 'stock_qty',
            },
            {
                Header: 'Action',
                accessor: 'action',
                disableFilters: true,
                disableSortBy: true
            },
        ], []
    );
    const INITIAL_FORM  = {
        id: "",
        category_id: "",
        name: "",
        description: "",
        stock_qty: "",
        price: "",
        weight: "",
        volume_length: "",
        volume_width: "",
        volume_height: "",
        selling_price_persen: ""
    }
    
    const [modalShow, setModalShow]         = useState(false);
    const [form, setForm]                   = useState(INITIAL_FORM);
    const [formOption, setFormOption]       = useState({ title: "Tambah Data", method: "POST" });
    const [formError, setFormError]         = useState({});
    const [isFormSubmit, setIsFormSubmit]   = useState(false);

    const [data, setData]           = useState([]);
    const [loading, setLoading]     = useState(false);
    const [reload, setReload]       = useState(0);
    const [pageCount, setPageCount] = useState(0);
    
    const fetchIdRef                = useRef(0);
    const photoRef             = useRef(null);
    
    const toggleModal = ({ resetForm = true } = {}) => {
        if (resetForm === true) {
            photoRef.current.value = "";
            setForm(INITIAL_FORM);
        }
        setFormError({});
        setModalShow(state => !state);
    }
    
    const openCreateModal = () => {
        setFormOption({ title: "Tambah Data", method: "POST" });
        toggleModal();
    }
    
    const openUpdateModal = async (id) => {
        MySwal.fire({
            title: "Menunggu",
            text: "Mengambil Data...",
            didOpen: () => {
                MySwal.showLoading()
            }
        });
        
        try {
            const request = await axios({
                url: API_URL + "?id=" + id,
                method: "GET"
            });

            const response = await request.data;
            MySwal.close();
            
            if (response.status_code === 200) {
                setFormOption({ title: "Edit Data", method: "PUT" });
                setForm({
                    id: response.data.id,
                    category_id: response.data.category_id,
                    name: response.data.name,
                    description: response.data.description,
                    stock_qty: response.data.stock_qty,
                    price: response.data.price,
                    weight: response.data.weight,
                    volume_length: response.data.volume_length,
                    volume_width: response.data.volume_width,
                    volume_height: response.data.volume_height,
                    is_werehouse: response.data.is_werehouse,
                    selling_price_persen: response.data.selling_price_persen
                })
                toggleModal({ resetForm: false });
            } else {
                const error         = new Error(response.status_code);
                error.status_code   = response.status_code;
                error.message       = response.message;

                throw error;
            }
        } catch (error) {
            toastAlert("error", error.message || "Internal Server Error!");
        }
    }

    const openDeleteModal = (id) => {
        MySwal.fire({
            title: 'Anda Yakin?',
            text: "Data Akan Dihapus Permanen!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
          }).then(async (result) => {
            if (result.isConfirmed) {
                MySwal.fire({
                    title: "Menunggu",
                    text: "Menghapus Data...",
                    didOpen: () => {
                        MySwal.showLoading()
                    }
                });
    
                try {
                    const request = await axios({
                        url: API_URL + "?id=" + id,
                        method: "DELETE"
                    });
        
                    const response = await request.data;
                    
                    if (response.status_code === 200) {
                        MySwal.close();
                        toastAlert("success", response.message, 2000);
                        setTimeout(() => {
                            refreshTable();
                        }, 2000);
                    } else {
                        const error         = new Error(response.status_code);
                        error.status_code   = response.status_code;
                        error.message       = response.message;
        
                        throw error;
                    }
                } catch (error) {
                    toastAlert("error", error.message || "Internal Server Error!");
                }
            }
        })
    }

    const formSubmitHandler = async (e) => {
        e.preventDefault();
        
        setIsFormSubmit(true);
        try {
            const url = (formOption.method === "PUT") 
                        ? API_URL + "?id=" + form.id + "&_method=put"
                        : API_URL;

            let formdata = new FormData();
            for (const [key, value] of Object.entries(form)) {
                formdata.append(key, value);
            }

            const request = await axios({
                url: url,
                method: "POST",
                data: formdata
            });

            const response = await request.data;

            if (response.status_code === 200) {
                toggleModal();
                toastAlert("success", response.message, 2000);
                setTimeout(() => {
                    refreshTable();
                }, 2000);
            } else {
                const error         = new Error(response.status_code);
                error.status_code   = response.status_code;
                error.message       = response.message;

                throw error;
            }
        } catch (error) {
            toastAlert("error", error.message || "Internal Server Error!");
        }
        setIsFormSubmit(false);
    }

    const refreshTable = () => {
        toastAlert("info", "Mengambil Data!", 1000);
        setReload(Math.random());
    }

    const fetchData   = useCallback(async ({ pageIndex, pageSize, filters }) => {
        const fetchId = ++fetchIdRef.current;
        setLoading(true);

        try {
            const page_query    = "?page=" + (pageIndex + 1);
            const limit_query   = "&limit=" + pageSize;
            let criteria        = "&criteria=";
            let criteria_query  = "";
            let i               = 1;
            for (const filter of filters) {
                // id === key;
                criteria_query += `${filter.id}:${filter.value}`;
                if (i !== filters.length) {
                    criteria_query += ",";
                } else {
                    criteria_query = criteria + criteria_query;
                }

                i++;
            }

            const request = await axios({
                method: "GET",
                url: API_URL + page_query + limit_query + criteria_query,
                cancelToken: fetchToken.token
            });

            const response = await request.data;

            if (response.status_code === 401) {
                await axios({
                    method: "GET",
                    url: "/api/logout"
                });

                router.push("/login");
            }

            if (fetchId === fetchIdRef.current & response.status_code === 200) {
                const resData       =   response.data;
                const readyData     =   resData.map((value, index) => {
                    const update_button =   (ACL.can_update === true)
                                                ?   <button onClick={() => openUpdateModal(value.id)} className="px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none">
                                                        <i className="fas fa-pencil-alt" />
                                                    </button>  :   null;
                    const delete_button =   (ACL.can_delete === true)
                                                ?   <button onClick={() => openDeleteModal(value.id)} className="px-4 py-2 bg-red-700 dark:bg-red-800 hover:bg-red-800 dark:hover:bg-red-700 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none">
                                                        <i className="fas fa-trash-alt" />
                                                    </button>  :   null;
                    value._index_   =   (index + 1) + (pageIndex * pageSize);
                    value.category_name   =   value.category.name;
                    value.price     =   new Intl.NumberFormat("id-ID", { style: 'currency', currency: 'IDR', }).format(value.price);
                    value.image     =   <img 
                                            className="w-16 h-auto rounded" 
                                            src={process.env.API_ENDPOINT + "uploads/" + value.image}
                                            onError={e => {
                                                e.target.onerror = null;
                                                e.target.src = "/img/avatar-1.png"
                                            }}
                                        />;
                    value.action    =   <div className="inline-flex gap-2">
                                            { update_button }
                                            { delete_button }
                                        </div>
                    return value;
                });
                
                setData(readyData);
                setPageCount(Math.ceil(response.total_data / pageSize));
                setLoading(false);
            } else {
                if (response.status_code !== 200) toastAlert("error", response.message);
                setData([]);
                setPageCount(0);
                setLoading(false);
            }
        } catch (error) {
            toastAlert("error", error.message);
        }
    }, []);

    useEffect(() => {
        return () => {
            fetchToken.cancel();
        }
    }, []);

    return (
        <Layout privilege={privilege}>
            <PageHeader title="Produk" />
            <section className="p-3 md:p-5">
                <div className="w-full rounded-lg bg-white dark:bg-gray-800 shadow p-5 md:p-8">
                    <div className="flex items-center justify-between mb-4">
                        <div className="flex-grow">
                            { (ACL.can_create === true) ? (
                                <button
                                    type="button"
                                    className="inline-flex items-center px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none"
                                    onClick={openCreateModal}
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" className="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
                                    </svg>
                                    <span className="mx-px">Tambah Data</span>
                                </button>
                            ) : null }
                        </div>
                        <button onClick={refreshTable} className="inline-flex items-center px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none">
                            <svg xmlns="http://www.w3.org/2000/svg" className="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M4 2a1 1 0 011 1v2.101a7.002 7.002 0 0111.601 2.566 1 1 0 11-1.885.666A5.002 5.002 0 005.999 7H9a1 1 0 010 2H4a1 1 0 01-1-1V3a1 1 0 011-1zm.008 9.057a1 1 0 011.276.61A5.002 5.002 0 0014.001 13H11a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0v-2.101a7.002 7.002 0 01-11.601-2.566 1 1 0 01.61-1.276z" clipRule="evenodd" />
                            </svg>
                        </button>
                    </div>
                    <div className="max-w-full">
                        <Datatable
                            columns={columns}
                            data={data}
                            fetchData={fetchData}
                            loading={loading}
                            reload={reload}
                            pageCount={pageCount}
                        />
                    </div>
                </div>
                <Modal title={formOption.title} isModalShow={modalShow} modalHandler={setModalShow}>
                    <form onSubmit={formSubmitHandler}>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Kategori</label>
                            <div className="col-span-4">
                                <select
                                    name="category_id"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, category_id: e.target.value })}
                                    value={form.category_id || ""}
                                >
                                    <option>-- Pilih --</option>
                                    { formData?.categories?.map(category => <option key={category.id} value={category.id}>{ category.name }</option>) }
                                </select>
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Nama</label>
                            <div className="col-span-4">
                                <input
                                    type="text"
                                    name="name"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, name: e.target.value })}
                                    value={form.name || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Deskripsi</label>
                            <div className="col-span-4">
                                <textarea
                                    name="name"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, description: e.target.value })}
                                    value={form.description || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Stok</label>
                            <div className="col-span-4">
                                <input
                                    type="number"
                                    min="0"
                                    name="stock_qty"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, stock_qty: e.target.value })}
                                    value={form.stock_qty || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Harga</label>
                            <div className="col-span-4">
                                <input
                                    type="number"
                                    min="0"
                                    name="price"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, price: e.target.value })}
                                    value={form.price || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Berat</label>
                            <div className="col-span-4">
                                <input
                                    type="number"
                                    min="0"
                                    name="weight"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, weight: e.target.value })}
                                    value={form.weight || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Panjang Volume</label>
                            <div className="col-span-4">
                                <input
                                    type="number"
                                    min="0"
                                    name="volume_length"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, volume_length: e.target.value })}
                                    value={form.volume_length || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Lebar Volume</label>
                            <div className="col-span-4">
                                <input
                                    type="number"
                                    min="0"
                                    name="volume_width"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, volume_width: e.target.value })}
                                    value={form.volume_width || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Tinggi Volume</label>
                            <div className="col-span-4">
                                <input
                                    type="number"
                                    min="0"
                                    name="volume_height"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, volume_height: e.target.value })}
                                    value={form.volume_height || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Selling Price</label>
                            <div className="col-span-4">
                                <input
                                    type="number"
                                    min="0"
                                    name="selling_price_persen"
                                    className="form-control"
                                    onChange={(e) => setForm({ ...form, selling_price_persen: e.target.value })}
                                    value={form.selling_price_persen || ""}
                                    required={true}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-1 md:grid-cols-6 mb-3">
                            <label className="text-gray-700 dark:text-gray-300 col-span-full md:col-span-2">Foto</label>
                            <div className="col-span-4">
                                <input
                                    type="file"
                                    name="imageImage"
                                    accept="image/*"
                                    ref={photoRef}
                                    className="form-control p-2"
                                    onChange={(e) => setForm({ ...form, imageImage:(e.target.files?.item(0) || null) })}
                                    required={formOption.method === "POST"}
                                />
                            </div>
                        </div>
                        <div className="inline-flex items-center space-x-2">
                            <button
                                type="submit"
                                className="px-4 py-2 bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-sm md:text-base text-gray-200 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none"
                                disabled={isFormSubmit}
                            >
                                { isFormSubmit 
                                    ? <i className="fas fa-sync fa-spin" />
                                    : "Kirim" }
                            </button>
                            <button
                                type="button"
                                className="px-4 py-2 bg-gray-100 dark:bg-gray-700 hover:bg-gray-200 dark:hover:bg-gray-600 text-sm md:text-base text-gray-500 dark:text-gray-300 font-semibold rounded-md shadow focus:outline-none"
                                onClick={toggleModal}
                                disabled={isFormSubmit}
                            >
                                Batal
                            </button>
                        </div>
                    </form>
                </Modal>
            </section>
        </Layout>
    );
}

// Ambil data session sebelum page di render terus pass ke props
export const getServerSideProps = withSession(async ({ res, req, resolvedUrl }) => {
    const session_data = req.session.get("session");
    
    if ( ! session_data) {
        return {
            redirect : {
                destination: "/login",
                statusCode: 302
            } 
        };
    }

    const { user, privilege }   = session_data;
    const current_privilege     = getCurrentPrivilege(resolvedUrl.substr(1), privilege);
    let formData                = {};
    try {
        const request = await axios({
            method: "GET",
            url: process.env.API_ENDPOINT + "api/category",
            headers: {
                "authKey": user.authKey
            }
        });

        const response = await request.data;

        if (response.status_code === 200) {
            formData.categories = response.data;
        }
    } catch (error) {
        
    }
    
    return {
        props: {
            user,
            privilege,
            formData,
            ACL: current_privilege,
        }
    };
});

export default Product;