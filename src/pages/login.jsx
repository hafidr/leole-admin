import { useState } from "react";
import { useRouter } from "next/router";
import Head from "next/head";
import Link from "next/link";
import axios from "axios";
import qs from "qs";
import { toastAlert } from "../lib/sweetalert";
import withSession from "../lib/session";

export const Login = () => {

    const router    = useRouter();
    const [form, setForm]                   = useState({});
    const [isFormSubmit, setIsFormSubmit]   = useState(false);

    const loginFormHandler = async (e) => {
        e.preventDefault();

        setIsFormSubmit(true);
        try {

            // Request ke route api, bukan langsung ke server
            // Set cookie gak bisa disini
            const request = await axios({
                method: "POST",
                url: "/api/login",
                timeout: 1000 * 30,
                timeoutErrorMessage: "Request Timeout: Silakan Coba Kembali!",
                data: qs.stringify({ ...form })
            });

            const response = await request.data;
            if (response.status_code === 200) {
                toastAlert("success", response.message, 2000);
                setTimeout(() => {
                    router.push("/");
                }, 2000);
            } else {
                toastAlert("error", response.message);
            }
        } catch (error) {
            toastAlert("error", error);
        }

        setIsFormSubmit(false);
    }

    return (
        <>
            <Head>
                <title>Login - { process.env.APP_NAME }</title>
            </Head>
            <div className="flex justify-center items-center w-full min-h-screen p-3 md:p-5 bg-gray-300">
                <div className="grid md:grid-cols-2 w-full md:w-2/3 shadow-lg">
                    <div className="hidden md:flex justify-center items-center w-full h-auto bg-indigo-800 p-5 rounded-l">
                        <img src="/img/auth/login_illustration.svg" />
                    </div>
                    <div className="flex flex-col w-full h-full py-5 px-5 md:px-10 bg-white rounded">
                        <div className="w-full">
                            <p className="text-2xl text-gray-700 font-semibold">Login</p>
                            <div className="w-1/6 h-1 bg-indigo-800 rounded-full my-2"></div>
                        </div>
                        <div className="w-full my-3">
                            <form className="space-y-3" onSubmit={loginFormHandler}>
                                <div className="flex flex-col">
                                    <input 
                                        type="email"
                                        name="email"
                                        placeholder="E-Mail"
                                        className="form-control"
                                        onChange={e => setForm({ ...form, email: e.target.value })}
                                        value={form.email || ""}
                                        required={true}
                                    />
                                </div>
                                <div className="flex flex-col">
                                    <input 
                                        type="password"
                                        name="password"
                                        placeholder="Password"
                                        className="form-control"
                                        onChange={e => setForm({ ...form, password: e.target.value })}
                                        value={form.password || ""}
                                        required={true} 
                                    />
                                </div>
                                <div className="flex">
                                    <Link href="/account/forgot-password">
                                        <span className="text-xs text-theme cursor-pointer">Lupa Password?</span>
                                    </Link>
                                </div>
                                <div className="flex flex-col">
                                    <button type="submit" className="inline-flex justify-center items-center w-full rounded-md border border-transparent px-4 py-2 bg-indigo-800 text-base leading-6 font-medium text-white shadow-sm focus:outline-none focus:border-indigo-900 focus:shadow-outline-red transition ease-in-out duration-150 sm:text-sm sm:leading-5" disabled={isFormSubmit}>
                                        { isFormSubmit ? (
                                            <>
                                                <i className="fas fa-sync fa-spin" />
                                                <span className="mx-2">Mencoba Masuk...</span>
                                            </>
                                        ) : "Login" }
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export const getServerSideProps = withSession(async ({ res, req }) => {
    const session_data = req.session.get("session");
    if (session_data) {
        return { 
            props: {},
            redirect : {
                destination: "/",
                statusCode: 302
            } 
        };
    }

    return { props: {} };
});

export default Login;