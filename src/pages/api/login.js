import withSession from "../../lib/session";
import axios from "axios";
import qs from "qs";

export default withSession(async (req, res) => {
    // Destruct variable
    const { email, password } = await req.body;
    const url = process.env.API_ENDPOINT + "api/login";

    try {
        const request = await axios({
            method: "POST",
            url: url,
            timeout: 1000 * 30,
            data: qs.stringify({ email, password })
        });

        const response = await request.data;
        
        if (response.status_code === 200) {
            const { data, message, status_code } = response;
            const privilege_data = data.privilege;

            // Set cuma property yg bakal kepake
            // Buat ngurangin size cookie
            const user_data = {
                id: data.id,
                name: data.name,
                email: data.email,
                photo: data.photo,
                authKey: data.authKey,
                role: data.role,
            }

            // Set session di cookie
            req.session.set("session", {
                user: user_data,
                privilege: privilege_data
            });

            // Save session & kirim response
            await req.session.save();
            res.json({data, message, status_code});
        } else {
            const error = {
                status_code: response.status_code,
                message: response.message
            };

            res.status(200).json(error);
        }
    } catch (error) {
        res.status(500).json("Internal Server Error");
    }
});