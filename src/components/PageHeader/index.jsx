import Head from "next/head";
import { useRouter } from "next/router";
import { Breadcrumb } from "../Breadcrumb"

export const PageHeader = ({ title }) => {
    
    const router = useRouter();
    const breadcrumbitems = (router.pathname !== "/") ? router.pathname.substr(1).split("/") : ["Dashboard"];

    return (
        <>
            <Head>
                <title>{ title + " - " + process.env.APP_NAME }</title>
            </Head>
            <header className="flex flex-nowrap items-center bg-gray-100 dark:bg-gray-800 shadow-lg px-5 py-5">
                <h1 className="flex flex-1 text-2xl md:text-3xl text-gray-800 dark:text-gray-300 font-bold">{ title }</h1>
                <Breadcrumb items={["Home", ...breadcrumbitems]} />
            </header>
        </>
    );
}

export default PageHeader;