export const Footer = () => {
    return (
        <footer className="flex flex-col md:flex-row justify-between w-full bg-main bg-gray-100 dark:bg-gray-900 text-gray-700 dark:text-gray-300 py-4 px-5 spaxe-y-2">
            <div className="flex justify-start w-full">
                Copyright &copy; { process.env.APP_NAME }
            </div>
            <div className="flex justify-start md:justify-end w-full">
                
            </div>
        </footer>
    );
}

export default Footer;