import { useState, useEffect, useMemo } from "react";
import { useTable, useSortBy, useFilters, usePagination } from "react-table";
import DefaultColumnFilter from "./DataFilter" ;
import classnames from "classnames";

export const Datatable = ({ columns, data, fetchData, loading, pageCount: controlledPageCount, reload }) => {
    
    const filterType = useMemo(
        () => [{
            text: (rows, id, filterValue) => {
                return rows.filter(row => {
                  const rowValue = row.values[id]
                  return rowValue !== undefined
                    ? String(rowValue)
                        .toLowerCase()
                        .startsWith(String(filterValue).toLowerCase())
                    : true
                })
            }
        }], []
    );

    const defaultColumn = useMemo(
        () => ({
          // Default Filter UI
          Filter: DefaultColumnFilter,
        }), []
    );

    const Pagination = () => {
        let startPage, endPage;

        if (pageCount <= 5) {
            startPage   = 1;
            endPage     = pageCount
        } else {
            let maxPagesBeforeCurrentPage   = Math.floor(4 / 2);
            let maxPagesAfterCurrentPage    = Math.ceil(6 / 2) - 1;

            if ((pageIndex + 1) <= maxPagesBeforeCurrentPage) {
                startPage   = 1;
                endPage     = 4;
            } else if ((pageIndex + 1) + maxPagesAfterCurrentPage >= pageCount) {
                startPage   = pageCount - pageCount + 1;
                endPage     = pageCount;
            } else {
                startPage   = (pageIndex + 1) - maxPagesBeforeCurrentPage;
                endPage     = (pageIndex + 1) + maxPagesAfterCurrentPage;
            }

        }

        let finalPages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

        return finalPages.map(value => {
            return (
                <button
                    key={value}
                    type="button"
                    className={
                        classnames("px-4 py-2 text-sm dark:text-gray-300 font-bold rounded", {
                            "bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-gray-100": value === (pageIndex + 1),
                            "bg-gray-200 dark:bg-gray-600 hover:bg-gray-300 dark:hover:bg-gray-500 text-gray-700": value !== (pageIndex + 1)
                        })
                    }
                    onClick={() => gotoPage(value - 1)}
                    disabled={(value + 1) === value}
                >
                    { value }
                </button>
            );
        });
    }

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page,
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        rows,
        setPageSize,
        state: { pageIndex, pageSize, filters },
    } = useTable({
            columns,
            data,
            defaultColumn,
            filterType,
            initialState: { filters: [], pageIndex: 0, pageSize: 10 },
            manualPagination: true,
            manualFilters: true,
            autoResetPage: false,
            pageCount: controlledPageCount
        },
        useFilters,
        useSortBy,
        usePagination,
    );

    useEffect(() => {
        fetchData({pageIndex, pageSize, filters});
    }, [fetchData, pageIndex, pageSize, filters, reload]);

    return (
        <div className="flex flex-col w-full">
            <div className="flex w-full flex-col md:flex-row justify-center md:justify-between">
                <div className="flex w-auto py-2 md:py-3 justify-center md:justify-start items-center">
                    <span className="text-sm text-gray-700 dark:text-gray-300 mr-2">Menampilkan:</span>
                    <select
                        className="form-control w-20 px-2 py-1"
                        value={pageSize}
                        onChange={e => {
                            setPageSize(Number(e.target.value))
                        }}
                        >
                        { [10, 20, 30, 40, 50].map(pageSize => (
                            <option key={pageSize} value={pageSize}>
                                {pageSize}
                            </option>
                        )) }
                    </select>
                    <span className="text-sm text-gray-700 dark:text-gray-300 ml-2">Data</span>
                </div>
            </div>
            <div className="flex max-w-full overflow-x-auto rounded">
                <table className="w-full border dark:border-gray-600 shadow border-collapse table table-auto" {...getTableProps()}>
                    <thead className="text-gray-200 dark:text-gray-300 bg-indigo-800 dark:bg-indigo-900">
                        { headerGroups.map((headerGroup, index) => (
                            <tr key={index} {...headerGroup.getHeaderGroupProps()}>
                                { headerGroup.headers.map((column, index) => (
                                    <th key={index} className="border table-cell dark:border-gray-600 px-4 py-3 text-left align-middle text-xs leading-4 font-medium uppercase tracking-wider select-none">
                                        <span className="block" {...column.getHeaderProps(column.getSortByToggleProps())}>
                                            { column.render('Header') }
                                            { column.canSort ? column.isSorted
                                            ? column.isSortedDesc
                                                ? <i className="inline ml-1 fas fa-sort-down fa-sm" />
                                                : <i className="inline ml-1 fas fa-sort-up fa-sm" />
                                            : <i className="inline ml-1 fas fa-sort fa-sm" /> : null }
                                        </span>
                                        { column.canFilter ? <div className="text-gray-700 dark:text-gray-300 mt-2">{ column.render('Filter') }</div> : null }
                                    </th>
                                )) }
                            </tr>
                        )) }
                    </thead>
                    <tbody {...getTableBodyProps()}>
                        { loading ? (
                            <tr className={
                                classnames({"hidden": !loading})
                            }>
                                <td colSpan="10000" className="text-gray-700 dark:text-gray-300 py-1 text-center">Memuat...</td>
                            </tr>
                        ) : null }
                        { (! loading && rows.length < 1) ? (
                            <tr>
                                <td colSpan="10000" className="text-gray-700 dark:text-gray-300 py-1 text-center">Data Kosong...</td>
                            </tr>
                        ) : null }
                        { rows.map((row, index) => {
                            prepareRow(row)
                            return (
                                <tr key={index} className={
                                    classnames({"hidden": loading})
                                } {...row.getRowProps()}>
                                    { row.cells.map(cell => {
                                        return <td className="table-cell text-gray-700 dark:text-gray-300 border dark:border-gray-600 px-5 md:px-3 py-3 whitespace-nowrap" {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                    }) }
                                </tr>
                            )
                        }) }
                    </tbody>
                </table>
            </div>
            <div className="flex flex-col md:flex-row justify-between items-center w-full space-y-2 py-3">
                <p className="flex text-sm text-gray-700 dark:text-gray-300 w-full">Menampilkan { pageSize } Data</p>
                <div className="flex max-w-full text-white justify-start md:justify-end space-x-2">
                    <button
                        type="button"
                        className={
                            classnames("px-4 py-2 text-sm bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-gray-100 dark:text-gray-300 font-bold rounded", {
                                "cursor-not-allowed": ! canPreviousPage
                            })
                        }
                        onClick={previousPage}
                        disabled={!canPreviousPage}
                    >
                        <i className="fas fa-chevron-left" />
                    </button>
                    <Pagination />
                    <button
                        type="button"
                        className={
                            classnames("px-4 py-2 text-sm bg-indigo-800 dark:bg-indigo-900 hover:bg-indigo-900 dark:hover:bg-indigo-800 text-gray-100 dark:text-gray-300 font-bold rounded", {
                                "cursor-not-allowed": ! canNextPage
                            })
                        }
                        onClick={nextPage}
                        disabled={!canNextPage}
                    >
                        <i className="fas fa-chevron-right" />
                    </button>
                </div>
            </div>
        </div>
    );

}

export default Datatable;