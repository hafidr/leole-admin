import { useState } from "react";
import { useAsyncDebounce } from 'react-table'

export const DefaultColumnFilter = ({ column: { filterValue, preFilteredRows, setFilter } }) => {
    const [value, setValue] = useState(filterValue);
    const onChange = useAsyncDebounce(value => {
        setFilter(value || undefined)
    }, 200);
    
    return (
        <input
            type="text"
            className="form-control py-1 z-40"
            value={value || ""}
            onChange={(e) => {
                setValue(e.target.value);
                onChange(e.target.value);
            }}
        />
    );
}

export default DefaultColumnFilter;