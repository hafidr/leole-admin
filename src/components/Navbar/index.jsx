import { useRouter } from "next/router";
import axios from "axios";
import classnames from "classnames";
import { toastAlert } from "../../lib/sweetalert";
import { useUser } from "../../lib/useUser";

export const Navbar = ({ sidebarToggle, isProfileDropdownShow, profileToggle }) => {

    const router = useRouter();
    const { session, mutateSession } = useUser();

    return (
        <nav className="flex items-center justify-between w-full h-16 px-5 bg-gray-100 dark:bg-gray-900 shadow">
            <div className="flex">
                <button onClick={sidebarToggle} className="focus:outline-none">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-gray-800 dark:text-gray-300" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h7" />
                    </svg>
                </button>
            </div>
            <div className="flex items-center flex-grow justify-end my-3 mx-5">
                <div className="flex items-center select-none cursor-pointer text-gray-700 dark:text-gray-300" onClick={profileToggle}>
                    <p className="hidden md:block tracking-wide font-medium mr-2">Hi, { session?.user?.name || ". . ." }</p>
                    <img src="/img/avatar-1.png" className="w-10 h-10 rounded-full" />
                    <i className="fas fa-chevron-down ml-3" />
                </div>
            </div>
            <div 
                className={
                    classnames("absolute top-2 right-0 mr-5 mt-16 overflow-hidden w-56 bg-white dark:bg-gray-600 rounded shadow-lg z-30 transition-max-height", {
                        "duration-300 ease-in max-h-96": isProfileDropdownShow,
                        "duration-200 ease-out max-h-0": ! isProfileDropdownShow
                    })
                }
            >
                <div className="px-5 py-3">
                    <h6 className="block pb-2 text-gray-700 dark:text-gray-300 tracking-wide font-medium border-b">Profile</h6>
                    <ul className="mt-5 mb-3">
                        <li>
                            <a
                                href="/api/login"
                                onClick={async (e) => {
                                    e.preventDefault();
                                    try {
                                        await mutateSession(
                                            axios({
                                                method: "POST",
                                                url: "/api/logout"
                                            })
                                        );

                                        router.push("/login");
                                    } catch (error) {
                                        toastAlert("Error", "Gagal Logout Akun!");
                                    }
                                }}
                                className="block px-4 py-2 text-gray-700 dark:text-gray-300 font-medium bg-gray-100 dark:bg-gray-700 rounded shadow-md"
                            >
                                Logout
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default Navbar;