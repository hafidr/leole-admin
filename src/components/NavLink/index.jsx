import Link from "next/link";
import classnames from "classnames";

export const NavLink = ({ title, href, isSidebarShow }) => {
    return (
        <li className="">
            <Link href={href}>
                <a className="flex items-center text-sm px-4 py-2 text-gray-300 rounded-md transition-colors duration-200 ease-in hover:bg-gray-800 cursor-pointer whitespace-nowrap">
                    <i className="fas fa-tachometer-alt fa-lg fa-fw"></i>
                    <div className={classnames({ "invisible": ! isSidebarShow })}>
                        <span className="font-semibold antialiased mx-2">{ title }</span>
                    </div>
                </a>
            </Link>
        </li>
    )
}