export const Breadcrumb = ({ items }) => {
    return (
        <div className="flex flex-1 h-full items-center justify-end">
            { items.map((item, index) => (
                <span key={index} className="breadcrumb-item text-xs text-gray-600 dark:text-gray-300 font-semibold mx-1 capitalize">{ item }</span>
            )) }
        </div>
    );
}