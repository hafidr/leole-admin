import classnames from "classnames";

export const Modal = ({ children, title, isModalShow, modalHandler, xl = false } = {}) => {

    return (
        <div className={classnames("fixed inset-0 overflow-y-auto", { "z-50": isModalShow, "-z-10": ! isModalShow })}>
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                <div className={
                    classnames("fixed inset-0 transition-opacity", {
                        "ease-out duration-300 opacity-100": isModalShow,
                        "ease-in duration-200 opacity-0": !isModalShow
                    })
                }>
                    <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
                </div>
                <span className="hidden sm:inline-block sm:align-middle sm:h-screen"></span>&#8203;
                <div 
                    className={
                        classnames(`inline-block align-bottom bg-white dark:bg-gray-800 rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle w-full`, {
                            "ease-out duration-300 opacity-100 translate-y-0 sm:scale-100": isModalShow,
                            "ease-in duration-200 opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95": !isModalShow,
                            "sm:max-w-4xl" : xl,
                            "sm:max-w-xl" : ! xl
                        })
                    }
                    role="dialog" 
                    aria-modal="true" 
                    aria-labelledby="modal-headline"
                >
                    <div className="bg-white dark:bg-gray-800 px-5 py-5">
                        <div className="relative py-3 sm:flex sm:items-start">
                            <button
                                type="button"
                                className="absolute inset-y-0 right-0 my-auto px-4 focus:outline-none"
                                onClick={() => modalHandler(false)}
                            >
                                <svg xmlns="http://www.w3.org/2000/svg" className="text-gray-600 dark:text-gray-300 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>
                            <h3 className="text-lg leading-6 font-medium text-gray-700 dark:text-gray-300" id="modal-headline">
                                { title }
                            </h3>
                        </div>
                        <div className="w-full md:mt-3 text-left">
                            { children }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Modal;