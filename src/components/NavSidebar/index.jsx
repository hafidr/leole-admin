import { useState } from "react";
import Link from "next/link";
import classnames from "classnames";
import { NavLink } from "../NavLink";
import toURLSlug from "../../lib/slugify";

export const NavSidebar = ({ isSidebarShow, menuLists }) => {

    const [activeDowpdown, setActiveDowpdown] = useState([]);

    const toggleDropdown = (index) => {
        if (activeDowpdown.includes(index)) {
            const newArray = activeDowpdown.filter(value => value !== index);
            setActiveDowpdown(newArray);
        } else {
            setActiveDowpdown([...activeDowpdown, index]);
        }
    }

    return (
        <ul className="flex flex-col my-4 space-y-2">
            <li className="">
                <Link href="/">
                    <a className="flex items-center text-sm px-4 py-2 text-gray-300 rounded-md transition-colors duration-200 ease-in hover:bg-gray-800 cursor-pointer whitespace-nowrap">
                        <i className="fas fa-tachometer-alt fa-lg fa-fw"></i>
                        <div className={classnames({ "invisible": ! isSidebarShow })}>
                            <span className="font-semibold antialiased mx-2">Dashboard</span>
                        </div>
                    </a>
                </Link>
            </li>
            { menuLists?.map(menu => {
                const hrefURL = `/${toURLSlug(menu.label)}`;

                if (menu.child !== undefined && menu.child.length > 0) {
                    return (
                        <li key={menu.id} className="">
                            <a onClick={() => toggleDropdown(menu.id)} className="flex items-center px-4 py-2 text-sm text-gray-300 rounded-md transition-colors duration-200 ease-in hover:bg-gray-800 cursor-pointer  whitespace-nowrap">
                                <i className="fas fa-users fa-lg fa-fw"></i>
                                <div className={classnames("flex flex-1 items-center", { "invisible": ! isSidebarShow })}>
                                    <span className="flex-grow font-semibold antialiased mx-2">{ menu.label }</span>
                                    <i className={classnames("fas fa-chevron-right fa-md fa-fw transition-transform", {"transform-gpu rotate-90": activeDowpdown.includes(menu.id)})}></i>
                                </div>
                            </a>
                            <div className={classnames("ml-4 shadow-inner overflow-hidden transition-max-height", {
                                "duration-500 ease-in max-h-96": activeDowpdown.includes(menu.id),
                                "duration-300 ease-out max-h-0": ! activeDowpdown.includes(menu.id)
                            })}>
                                <ul className="flex flex-1 justify-items-end flex-col mt-1 space-y-2">
                                    { menu.child.map(children => {
                                        const hrefURL = `/${toURLSlug(children.label)}`;
                                        if (children.child !== undefined && children.child.length > 0) {
                                            return (
                                                <li key={children.id} className="">
                                                    <a onClick={() => toggleDropdown(children.id)} className="flex items-center px-4 py-2 text-sm text-gray-300 rounded-md transition-colors duration-200 ease-in hover:bg-gray-800 cursor-pointer  whitespace-nowrap">
                                                        <i className="fas fa-users fa-lg fa-fw"></i>
                                                        <div className={classnames("flex flex-1 items-center", { "invisible": ! isSidebarShow })}>
                                                            <span className="flex-grow font-semibold antialiased mx-2">{ children.label }</span>
                                                            <i className={classnames("fas fa-chevron-right fa-md fa-fw transition-transform", {"transform-gpu rotate-90": activeDowpdown.includes(children.id)})}></i>
                                                        </div>
                                                    </a>
                                                    <div className={classnames("ml-4 shadow-inner overflow-hidden transition-max-height", {
                                                        "duration-500 ease-in max-h-96": activeDowpdown.includes(children.id),
                                                        "duration-300 ease-out max-h-0": ! activeDowpdown.includes(children.id)
                                                    })}>
                                                        <ul className="flex flex-1 justify-items-end flex-col mt-1 space-y-2">
                                                            { children.child.map(grandchild => {
                                                                const hrefURL = `/${toURLSlug(grandchild.label)}`;
                                                                if (grandchild.child !== undefined && grandchild.child.length > 0) {
                                                                    return (
                                                                        <li key={grandchild.id} className="">
                                                                            <a onClick={() => toggleDropdown(grandchild.id)} className="flex items-center px-4 py-2 text-sm text-gray-300 rounded-md transition-colors duration-200 ease-in hover:bg-gray-800 cursor-pointer  whitespace-nowrap">
                                                                                <i className="fas fa-users fa-lg fa-fw"></i>
                                                                                <div className={classnames("flex flex-1 items-center", { "invisible": ! isSidebarShow })}>
                                                                                    <span className="flex-grow font-semibold antialiased mx-2">{ grandchild.label }</span>
                                                                                    <i className={classnames("fas fa-chevron-right fa-md fa-fw transition-transform", {"transform-gpu rotate-90": activeDowpdown.includes(grandchild.id)})}></i>
                                                                                </div>
                                                                            </a>
                                                                            <div className={classnames("ml-4 shadow-inner overflow-hidden transition-max-height", {
                                                                                "duration-500 ease-in max-h-96": activeDowpdown.includes(grandchild.id),
                                                                                "duration-300 ease-out max-h-0": ! activeDowpdown.includes(grandchild.id)
                                                                            })}>
                                                                                <ul className="flex flex-1 justify-items-end flex-col mt-1 space-y-2">
                                                                                    { grandchild.child.map(greateGrandchild => {
                                                                                        const hrefURL = `/${toURLSlug(greateGrandchild.label)}`;
                                                                                        return (
                                                                                            <li key={greateGrandchild.id}>
                                                                                                <Link href={hrefURL}>
                                                                                                    <a href={hrefURL} className="flex items-center text-sm px-4 py-2 text-gray-300 rounded-md transition-colors duration-200 ease-in hover:bg-gray-800 cursor-pointer  whitespace-nowrap">
                                                                                                        <i className="fas fa-tachometer-alt fa-lg fa-fw"></i>
                                                                                                        <div className={classnames({ "invisible": ! isSidebarShow })}>
                                                                                                            <span className="font-semibold antialiased mx-2">{ greateGrandchild.label }</span>
                                                                                                        </div>
                                                                                                    </a>
                                                                                                </Link>
                                                                                            </li>
                                                                                        );
                                                                                    }) }
                                                                                </ul>
                                                                            </div>
                                                                        </li>
                                                                    );
                                                                } else {
                                                                    return (
                                                                        <li key={grandchild.id}>
                                                                            <Link href={hrefURL}>
                                                                                <a href={hrefURL} className="flex items-center text-sm px-4 py-2 text-gray-300 rounded-md transition-colors duration-200 ease-in hover:bg-gray-800 cursor-pointer  whitespace-nowrap">
                                                                                    <i className="fas fa-tachometer-alt fa-lg fa-fw"></i>
                                                                                    <div className={classnames({ "invisible": ! isSidebarShow })}>
                                                                                        <span className="font-semibold antialiased mx-2">{ grandchild.label }</span>
                                                                                    </div>
                                                                                </a>
                                                                            </Link>
                                                                        </li>
                                                                    );
                                                                }
                                                            }) }
                                                        </ul>
                                                    </div>
                                                </li>
                                            );
                                        } else {
                                            return (
                                                <li key={children.id}>
                                                    <Link href={hrefURL}>
                                                        <a href={hrefURL} className="flex items-center text-sm px-4 py-2 text-gray-300 rounded-md transition-colors duration-200 ease-in hover:bg-gray-800 cursor-pointer  whitespace-nowrap">
                                                            <i className="fas fa-tachometer-alt fa-lg fa-fw"></i>
                                                            <div className={classnames({ "invisible": ! isSidebarShow })}>
                                                                <span className="font-semibold antialiased mx-2">{ children.label }</span>
                                                            </div>
                                                        </a>
                                                    </Link>
                                                </li>
                                            );
                                        }
                                    }) }
                                </ul>
                            </div>
                        </li>
                    );
                } else {
                    return (
                        <NavLink
                            key={menu.id}
                            title={menu.label}
                            href={hrefURL}
                            isSidebarShow
                        />
                    )
                }

            }) }
        </ul>
    );
}

export default NavSidebar;