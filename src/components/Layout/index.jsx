import { useState, useEffect } from "react";
import Sidebar from "../Sidebar";
import Footer from "../Footer";
import Navbar from "../Navbar"

export const Layout = ({ privilege, children }) => {
    
    const [sidebarShow, setSidebarShow]                 =   useState(true);
    const [profileDropdownShow, setProfileDropdownShow] =   useState(false);
    const sidebarToggleHandler = () => {
        setSidebarShow(prevState => ! prevState);
    }
    const profileDropdownToggleHandler = () => {
        setProfileDropdownShow(prevState => ! prevState);
    }

    useEffect(() => {
        if (document.documentElement.clientWidth <= 640) setSidebarShow(false);
    }, []);

    return (
        <>
            <div className="relative flex max-w-full h-screen bg-gray-300 dark:bg-gray-900 dark:shadow-inner">
                <Sidebar
                    isSidebarShow={sidebarShow}
                    sidebarToggle={sidebarToggleHandler}
                    menuLists={privilege}
                />
                <div className="flex flex-col w-full">
                    <Navbar sidebarToggle={sidebarToggleHandler} isProfileDropdownShow={profileDropdownShow} profileToggle={profileDropdownToggleHandler} />
                    <div className="relative flex flex-col h-screen overflow-x-hidden overflow-y-auto">
                        <main className="flex-grow">
                            { children }
                        </main>
                        <Footer />
                    </div>
                </div>
            </div>
        </>
    );
}

export default Layout;