import { useState, useEffect } from "react";
import classnames from "classnames";
import useDarkMode from 'use-dark-mode';
import NavSidebar from "../NavSidebar";

export const Sidebar = ({ isSidebarShow, sidebarToggle, menuLists }) => {

    const [isDarkMode, setIsDarkMode] = useState(false);
    const darkMode = useDarkMode(true, {
        classNameDark: "dark",
        classNameLight: "light"
    });

    useEffect(() => {
        setIsDarkMode(darkMode.value);
    }, [darkMode]);

    return (
        <>
            <aside 
                className={classnames("transition-width h-screen bg-gray-900 dark:bg-gray-900 overflow-x-hidden overflow-y-auto shadow-lg dark:shadow-2xl z-50 absolute md:relative", {
                    "duration-500 ease-in w-80": isSidebarShow,
                    "duration-300 ease-out w-0": ! isSidebarShow
                })}
            >
                <nav className="flex flex-col flex-nowrap px-2">
                    <div className="flex items-center justify-between flex-nowrap w-full h-16 px-5 whitespace-nowrap">
                        <h4 className="text-gray-200 text-2xl font-semibold tracking-wide uppercase">Leole</h4>
                        <div className="flex justify-around">
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 3v1m0 16v1m9-9h-1M4 12H3m15.364 6.364l-.707-.707M6.343 6.343l-.707-.707m12.728 0l-.707.707M6.343 17.657l-.707.707M16 12a4 4 0 11-8 0 4 4 0 018 0z" />
                            </svg>
                            <div onClick={darkMode.toggle} className="flex items-center w-8 h-5 bg-gray-700 rounded-full p-1 mx-1">
                                <button
                                    type="button"
                                    onClick={darkMode.toggle}
                                    className={classnames("transition-transform transform duration-200 bg-gray-400 w-4 h-4 rounded-full focus:outline-none shadow cursor-pointer", {
                                        "translate-x-2": isDarkMode
                                    })}
                                >

                                </button>
                            </div>
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z" />
                            </svg>
                        </div>
                    </div>
                    <NavSidebar isSidebarShow={isSidebarShow} menuLists={menuLists} />
                </nav>
            </aside>
            <button onClick={sidebarToggle} className={classnames("absolute inset-0 w-full h-screen bg-gray-900 bg-opacity-70 z-40 md:hidden", {"hidden": ! isSidebarShow})}></button>
        </>
    );
}

export default Sidebar;