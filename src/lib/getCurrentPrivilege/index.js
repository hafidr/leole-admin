import toURLSlug from "../slugify";
import _ from "underscore";

export const getCurrentPrivilege = (currentURL, privileges) => {
    const trimmedURL = toURLSlug(currentURL);

    function searchCurrentPrivilege(privileges) {
        for (const privilege of privileges) {
            if (privilege.label.toLowerCase() === trimmedURL && (privilege.child === undefined || privilege.child.length < 1)) {
                return privilege;
            } else {
                for (const privilege_child of privilege.child) {
                    if (privilege_child.label.toLowerCase() === trimmedURL) return privilege_child;
                    searchCurrentPrivilege(privilege_child);
                }
            }
            
        }

        return null;
    }

    const privilege_data = searchCurrentPrivilege(privileges);
    return privilege_data || {
        can_create: false,
        can_read: false,
        can_update: false,
        can_delete: false
    };
    
}

export default getCurrentPrivilege;