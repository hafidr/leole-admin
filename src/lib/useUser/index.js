import { useEffect } from "react";
import { useRouter } from "next/router";
import useSWR from "swr";

export const useUser = ({ redirectIfFound = false, redirectTo = false } = {}) => {
    const router = useRouter();
    const { data: session, mutate: mutateSession } = useSWR("/api/user");
    
    useEffect(() => {
        if (!session && !redirectTo) return;
        if (
            (redirectTo && !redirectIfFound && !session?.isLoggedIn) ||
            (redirectIfFound && session?.isLoggedIn)
        ) {
            router.push(redirectTo);
        }

    }, [session, mutateSession, redirectIfFound, redirectTo]);

    return { session, mutateSession }
}

export default useUser;