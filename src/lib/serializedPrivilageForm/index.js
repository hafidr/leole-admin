import _ from "underscore";

export default function serializedPriviageForm(privileges) {

    let serialized = [];

    function serializedForm(form) {
        for (const privilege of form) {
            if (privilege.child.length < 1 && _.findWhere(serialized, { menu_id: privilege.menu_id }) === undefined) {
                serialized.push({
                    menu_id: privilege.menu_id,
                    label: privilege.label,
                    can_create: privilege.can_create,
                    can_read: privilege.can_read,
                    can_update: privilege.can_update,
                    can_delete: privilege.can_delete
                });
            } else {
                for (const children of privilege.child) {
                    serialized.push({
                        is_parent: true,
                        menu_id: children.menu_id,
                        label: children.label,
                        can_create: children.can_create,
                        can_read: children.can_read,
                        can_update: children.can_update,
                        can_delete: children.can_delete
                    });
                }

                serializedForm(privilege.child);
            }
        }

        return serialized;
    }
    
    serializedForm(privileges);
    return serialized;
}