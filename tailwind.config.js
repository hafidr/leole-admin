module.exports = {
  purge: [
    './src/components/**/*.jsx',
    './src/pages/**/*.jsx'
  ],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      minHeight: {
        8: '2rem'
      },
      transitionProperty: {
        'width': 'width',
        'max-height': 'max-height'
      },
      zIndex: {
        "-10": "-10"
      }
    },
  },
  variants: {
    extend: {
      boxShadow: ["dark"]
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
  ],
}
